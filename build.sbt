organization := "com.typesafe.training"

name := "hakky-hour"

version := "3.0.0"

scalaVersion := Version.scala

libraryDependencies ++= Dependencies.hakkyHour

scalacOptions ++= List(
  "-unchecked",
  "-deprecation",
  "-Xlint",
  "-language:_",
  "-target:jvm-1.6",
  "-encoding", "UTF-8"
)

retrieveManaged := true

parallelExecution in Test := false

(initialCommands in Compile) := """import akka.actor._
                                  |import com.typesafe.training.hakkyhour._
                                  |import scala.concurrent._
                                  |import scala.concurrent.duration._""".stripMargin
