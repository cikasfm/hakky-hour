package com.typesafe.training.hakkyhour

import scala.concurrent.duration.FiniteDuration

import HakkyHour.NoMoreDrinks
import Waiter.{ DrinkServed, ServeDrink }
import akka.actor.{ Actor, ActorLogging, ActorRef, Props, actorRef2Scala }

object Guest {

  private case object DrinkFinished

  case class DrunkException(drinkCount: Int) extends Exception("Too many drinks!")

  def props(favoriteDrink: Drink, waiter: ActorRef, finishDrinkDuration: FiniteDuration, isStubborn: Boolean, maxDrinkCount: Int): Props =
    Props(new Guest(favoriteDrink, waiter, finishDrinkDuration, isStubborn, maxDrinkCount))
}

class Guest(favoriteDrink: Drink, waiter: ActorRef, finishDrinkDuration: FiniteDuration, isStubborn: Boolean, maxDrinkCount: Int) extends Actor with ActorLogging {

  import Guest._
  import Waiter._
  import HakkyHour._
  import context.dispatcher

  waiter ! ServeDrink(favoriteDrink)

  var drinkCount: Int = 0;

  override def receive: Receive = {
    case DrinkServed(drink) =>
      if (drink != favoriteDrink) {
        sender ! Complaint(favoriteDrink)
        log.info("Wrong drink {}! I've asked for {}!", drink, favoriteDrink)
      } else {
        drinkCount += 1
        log.info("Enjoying my {}. yummy {}!", drinkCount, drink)
        context.system.scheduler.scheduleOnce(
          finishDrinkDuration,
          self,
          DrinkFinished
        )
      }
    case DrinkFinished =>
      if (drinkCount >= maxDrinkCount) {
        throw new DrunkException(drinkCount)
      }
      orderAnotherDrink()
    case NoMoreDrinks if isStubborn =>
      orderAnotherDrink()
    case NoMoreDrinks =>
      log.info("All right, time to go home!")
      context.stop(self)
  }

  override def postStop() {
    log.info("Good Bye!")
  }

  def orderAnotherDrink(): Unit = {
    log.info("Give me another one!")
    waiter ! ServeDrink(favoriteDrink)
  }

}