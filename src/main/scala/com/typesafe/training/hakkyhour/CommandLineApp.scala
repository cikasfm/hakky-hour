/*
 * Copyright © 2012 Typesafe, Inc. All rights reserved.
 */

package com.typesafe.training.hakkyhour

import scala.annotation.tailrec

import HakkyHour.CreateGuest
import akka.actor.{ ActorSystem, actorRef2Scala }
import akka.event.Logging

object CommandLineApp extends App with CommandParser {

  import Command._
  import HakkyHour._

  val system = ActorSystem("hakky-hour-system")
  val log = Logging(system, getClass.getName)
  val hakkyHour = system.actorOf(HakkyHour.props, "z-hakky-hour")

  log.warning("CommandLineApp started: Enter commands into the terminal")
  commandLoop()
  system.awaitTermination()

  @tailrec
  def commandLoop(): Unit =
    parseCommand(readLine()) match {
      case GuestCommand(count, drink, isStubborn, maxDrinkCount) =>
        for (_ <- 1 to count) hakkyHour ! CreateGuest(drink, maxDrinkCount, isStubborn)
        commandLoop()
      case QuitCommand =>
        system.shutdown()
      case IllegalCommand =>
        log.warning("Illegal command!")
        commandLoop()
    }
}
