package com.typesafe.training.hakkyhour

import akka.actor.{ Actor, Props }
import akka.actor.ActorRef

object Waiter {

  case class DrinkServed(drink: Drink)

  case class ServeDrink(drink: Drink)

  case class Complaint(drink: Drink)

  case class FrustratedException(drink: Drink, guest: ActorRef) extends Exception("Too many complaints!")

  def props(hakkyHour: ActorRef, barKeeper: ActorRef, maxComplaintCount: Int): Props =
    Props(new Waiter(hakkyHour, barKeeper, maxComplaintCount))

}

class Waiter(hakkyHour: ActorRef, barKeeper: ActorRef, maxComplaintCount: Int) extends Actor {

  import Waiter._
  import BarKeeper._

  var numOfComplaints: Int = 0;

  override def receive: Receive = {
    case ServeDrink(drink) => {
      hakkyHour ! PrepareDrink(drink, sender)
    }
    case DrinkPrepared(drink, guest) => {
      guest ! DrinkServed(drink)
    }
    case Complaint(drink) if numOfComplaints > maxComplaintCount => {
      throw new FrustratedException(drink, sender)
    }
    case Complaint(drink) => {
      numOfComplaints += 1
      barKeeper ! PrepareDrink(drink, sender)
    }
  }

}