package com.typesafe.training.hakkyhour

import scala.concurrent.duration.FiniteDuration
import scala.util.Random

import akka.actor.{ Actor, ActorRef, Props, actorRef2Scala }

object BarKeeper {

  case class PrepareDrink(drink: Drink, guest: ActorRef)

  case class DrinkPrepared(drink: Drink, guest: ActorRef)

  def props(prepareDrinkDuration: FiniteDuration, accuracy: Int): Props =
    Props(new BarKeeper(prepareDrinkDuration, accuracy))
}

class BarKeeper(prepareDrinkDuration: FiniteDuration, accuracy: Int) extends Actor {

  import BarKeeper._
  import Drink._

  override def receive: Receive = {
    case PrepareDrink(drink, guest) => {
      var prepared: Drink = null;
      if (Random.nextInt(100) < accuracy) {
        // prepare correct drink
        prepared = drink;
      } else {
        // prepare a wrong drink
        if (drink == Akkarita) {
          prepared = PinaScalada
        } else if (drink == PinaScalada) {
          prepared = MaiPlay
        } else {
          prepared = Akkarita
        }
      }
      busy(prepareDrinkDuration)
      sender ! DrinkPrepared(prepared, guest)
    }
  }

}