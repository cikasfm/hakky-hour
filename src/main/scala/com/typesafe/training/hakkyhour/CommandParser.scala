/*
 * Copyright © 2012 Typesafe, Inc. All rights reserved.
 */

package com.typesafe.training.hakkyhour

import scala.util.parsing.combinator.RegexParsers

trait CommandParser {

  object Parser extends RegexParsers {

    import Command._

    val guestCommand =
      opt(int) ~ (guest ~> opt(drink) ~ opt(stubborn) ~ opt(int)) ^^ {
        case count ~ (drink ~ stubborn ~ maxDrinkCount) =>
          GuestCommand(
            count getOrElse 1,
            drink getOrElse Drink.Akkarita,
            stubborn.isDefined,
            maxDrinkCount getOrElse Int.MaxValue
          )
      }

    val quitCommand = quit ^^ (_ => QuitCommand)

    val commands = guestCommand | quitCommand

    def parseCommand(command: String): Command =
      parseAll(commands, command) match {
        case Success(command, _) => command
        case _                   => IllegalCommand
      }

    def guest: Parser[String] = """guest|g""".r

    def drink: Parser[Drink] = """a|A|m|M|p|P""".r ^^ Drink.apply

    def stubborn: Parser[Any] = """s""".r

    def int: Parser[Int] = """\d+""".r ^^ (_.toInt)

    def quit: Parser[String] = """quit|q""".r

  }

  sealed trait Command

  object Command {

    case class GuestCommand(count: Int, drink: Drink, isStubborn: Boolean, maxDrinkCount: Int) extends Command

    case object QuitCommand extends Command

    case object IllegalCommand extends Command
  }

  def parseCommand(command: String): Command = {
    println("You typed:" + command)
    Parser.parseCommand(command)
  }
}
