package com.typesafe.training.hakkyhour

import scala.concurrent.duration.{ Duration, FiniteDuration, MILLISECONDS }
import akka.actor.{ Actor, ActorLogging, ActorRef, OneForOneStrategy, PoisonPill, Props, SupervisorStrategy, Terminated }
import akka.routing.FromConfig

object HakkyHour {

  case class CreateGuest(favoriteDrink: Drink, maxDrinkCount: Int, isStubborn: Boolean)

  case object NoMoreDrinks

  def props: Props =
    Props(new HakkyHour)
}

class HakkyHour extends Actor with ActorLogging {

  import HakkyHour._
  import BarKeeper._

  val maxDrinkCount: Int = context.system.settings.config.getInt("hakky-hour.max-drink-count");
  val barKeeperAccuracy: Int = context.system.settings.config.getInt("hakky-hour.barkeeper.accuracy");
  val maxComplaintCount: Int = context.system.settings.config.getInt("hakky-hour.waiter.max-complaint-count");

  val drinkFinishDuration: FiniteDuration = Duration(
    context.system.settings.config.getMilliseconds("hakky-hour.guest.finish-drink-duration"),
    MILLISECONDS
  )

  val prepareDrinkDuration: FiniteDuration = Duration(
    context.system.settings.config.getMilliseconds("hakky-hour.guest.finish-drink-duration"),
    MILLISECONDS
  )

  val barKeeper = context.actorOf(BarKeeper.props(prepareDrinkDuration, barKeeperAccuracy).withRouter(FromConfig()), "z-bar-keeper")
  val waiter = context.actorOf(Waiter.props(self, barKeeper, maxComplaintCount), "z-waiter");

  var guestDrinkCount = Map.empty[ActorRef, Int]

  override def receive: Receive = {
    case CreateGuest(favoriteDrink, maxDrinkCount, isStubborn) =>
      val guest = context.actorOf(Guest.props(favoriteDrink, waiter, drinkFinishDuration, isStubborn, maxDrinkCount))
      context.watch(guest)
    case CreateGuest => // TODO
    case prepareDrink @ PrepareDrink(_, guest) =>
      val drinkCount = guestDrinkCount.getOrElse(guest, 0);
      if (drinkCount < maxDrinkCount) {
        guestDrinkCount += guest -> (drinkCount + 1)
        barKeeper forward prepareDrink
      } else if (drinkCount == maxDrinkCount) {
        log.info("Sorry, {}, but we won't serve you more than {} drinks", guest.path.name, maxDrinkCount)
        guestDrinkCount += guest -> (drinkCount + 1)
        guest ! NoMoreDrinks
      } else {
        log.info("Sorry guest, {}, you had enough drinks, sending you the Poison Pill!", guest.path.name)
        guest ! PoisonPill
      }
    case Terminated(guest) =>
      if (guestDrinkCount.contains(guest)) {
        guestDrinkCount -= guest
        log.info("Thanks, {}, for being our guest!", guest.path.name)
      }
  }

  override def supervisorStrategy: SupervisorStrategy =
    OneForOneStrategy() {
      case Guest.DrunkException(_) => SupervisorStrategy.Stop
      case Waiter.FrustratedException(drink, guest) => {
        val waiter = sender
        barKeeper.tell(PrepareDrink(drink, guest), waiter)
        SupervisorStrategy.Restart
      }
    }

}