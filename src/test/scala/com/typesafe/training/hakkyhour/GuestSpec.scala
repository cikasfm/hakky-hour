package com.typesafe.training.hakkyhour

import scala.concurrent.duration._
import org.scalatest.{ BeforeAndAfterAll, Matchers, WordSpecLike }
import Waiter.DrinkServed
import akka.actor.ActorSystem
import akka.testkit.{ TestActorRef, TestKit }
import akka.testkit.ImplicitSender

class GuestSpec extends TestKit(ActorSystem("z-test-system")) with WordSpecLike with Matchers with BeforeAndAfterAll with ImplicitSender {

  import Waiter._

  "Sending DrinkServed to a Guest" should {
    "increase its drinkCount" in {
      val guest = TestActorRef(new Guest(Drink.Akkarita, system.deadLetters, 100 milliseconds, false, 2))
      guest.underlyingActor.drinkCount shouldEqual 0
      guest ! DrinkServed(Drink.Akkarita)
      guest.underlyingActor.drinkCount shouldEqual 1
    }
  }

  "Creating a Guest" should {
    "result in sending ServeDrink to its waiter" in {
      val guest = system.actorOf(Guest.props(Drink.Akkarita, testActor, 100 milliseconds, false, 2))
      expectMsg(ServeDrink(Drink.Akkarita))
    }
  }

  "Sending DrinkServed to a Guest" should {
    "result in sending ServeDrink to its waiter after finishDrinkDuration" in {
      val guest = system.actorOf(Guest.props(Drink.Akkarita, testActor, 100 milliseconds, false, 2))
      expectMsg(ServeDrink(Drink.Akkarita))
      within(100 milliseconds, 500 milliseconds) {
        guest ! DrinkServed(Drink.Akkarita)
        expectMsg(ServeDrink(Drink.Akkarita))
      }
    }
  }

  override protected def afterAll() = shutdown(system)

}