package com.typesafe.training.hakkyhour

import scala.concurrent.duration._

import org.scalatest.{ BeforeAndAfterAll, Matchers, WordSpecLike }

import akka.actor.ActorSystem
import akka.testkit.ImplicitSender
import akka.testkit.TestKit

class BarkeeperSpec extends TestKit(ActorSystem("z-barkeeper-sec-test"))
    with ImplicitSender with WordSpecLike with Matchers with BeforeAndAfterAll {

  import BarKeeper._

  "Sending PrepareDrink to a Barkeeper" should {
    "result in a DrinkPrepared response after prepareDrinkDuration" in {
      val barkeeperRef = system.actorOf(BarKeeper.props(100 milliseconds, 100))
      val guest = system.deadLetters
      within(100 milliseconds, 500 milliseconds) {
        barkeeperRef ! PrepareDrink(Drink.Akkarita, guest)
        expectMsg(DrinkPrepared(Drink.Akkarita, guest))
      }
    }
  }

  override protected def afterAll() = shutdown(system)

}